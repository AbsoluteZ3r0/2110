/*
 * main.cxx
 * 
 * Copyright 2014 Alex <alex@Linux>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */


#include <iostream>
#include <string>
#include <cstdlib>
#include <cstring>						//for dat memset

using namespace std;


/*TODO:
 * put tuples and tables into their own classes
 * Parse user input and compare to see if the input is meaningful (fits parameters)
 * setup input prompt
 */
class Students{
	
};
char parser(string userStuff);					//userInput gets sent here to be parsed
int main(int argc, char **argv)
{
	cout << "------------------------------------" << endl;
	cout << "|	     A program by           |" << endl;
	cout << "|           Alexander Fatum         |" << endl;
	cout << "------------------------------------" << endl;

	cout << endl;
	cout << endl;
	cout << endl;
/////////////////////////////////////////////////////////////
	
	string userInput;					//what the user inputs
	string quit;						//the flag to end the program
	cout << "Type 'help' for a list of commands." << endl;
	cout << "Or just go ahead and type." << endl;
	
	while(userInput.substr(0,4) != "quit"){
		cout << "tables> ";
		getline(cin, userInput, '\n');
//DB		cout << userInput << "  FO SHO" << endl;
		if((userInput.substr(0,12) == "students add") || (userInput.substr(0,15) == "students delete") || (userInput.substr(0,16) == "students display") || (userInput.substr(0,10) == "grades add") || (userInput.substr(0,13) == "grades delete") || (userInput.substr(0,15) == "grades display") || (userInput.substr(0,4) == "quit")){
			parser(userInput);
		}
		else
			cout << "Invalid Input" << endl;
	}
	return 0;
}

char parser(string userStuff){					//parses userInput
	int studentID;						//the student's ID
	string buffy;						//Oh look! It's buffy the buffer slayer	
	string grade;						//the student's grade
	string cN;						//name of student's class
	int counter;						//a counter	
	int counter2;						//a counter	
	int switchy = 0;					//a flag/switch	
	char check;						//a checker	
	int counter3;						//a counter	
	

	if(userStuff.substr(0,4) == "quit")
		return 0;
	else if(userStuff.substr(0,12) == "students add"){
		counter = userStuff.length();
		if(counter > 12){		
			buffy = userStuff.substr(13,3);
			studentID = atoi(buffy.c_str());		//converting string to num
			cout << "NUMBER:" << studentID << endl;
		}
		else
			cout << "ERROR: Enter more arguments." << endl;
	}

	else if(userStuff.substr(0,15) == "students delete"){
		counter = userStuff.length();
		if(counter > 15){
			buffy = userStuff.substr(16,19);
			studentID = atoi(buffy.c_str());
			cout << "NUMBER:" << studentID << endl;
		}
		else
			cout << "ERROR: Enter more arguments." << endl;

	}
	else if(userStuff.substr(0,16) == "students display"){
//OUTPUT STUDENTS
	}
	else if(userStuff.substr(0,10) == "grades add"){
		counter3 = userStuff.length();
		if(counter3 > 10){		
			buffy = userStuff.substr(11,15);
			studentID = atoi(buffy.c_str());
//DB			cout << "NUMBER:" << studentID << endl;
//DB			buffy = "";					//setting buffy to null
			counter = 15;					//setting counter to after the studentID
			counter2 = userStuff.length();
//DB			cout << counter2 << endl;
			counter = counter2 - 15;
//DB			cout << "Counter:" << counter << endl;
			cN = userStuff.substr(15, counter-2);
			grade = userStuff.substr(counter2-1,1);
//DB			cout << "CLASSNAME:" << cN << endl;
//DB			cout << "GRADE:" << grade << endl;
		}
		else
			cout << "ERROR: Enter more arguments." << endl;

	} 
	else if(userStuff.substr(0,13) == "grades delete"){
		counter = userStuff.length();
		if(counter > 13){		
			buffy = userStuff.substr(14,3);
			studentID = atoi(buffy.c_str());		//converting string to num
//DB			cout << "NUMBER:" << studentID << endl;
		}
		else
			cout << "ERROR: Enter more arguments." << endl;
	}
	else if(userStuff.substr(0,15) == "grades display"){
//output grades
	}
	else if(userStuff.substr(0,4) == "quit"){
		return 1;
	}
	else
		cout << "Not sure how you got this far, but you have a command that can not do anything." << endl;
//DB	cout << userStuff << endl;
//DB	cout << "lolwut" << endl;
	return 0;

}
